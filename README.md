# Stock table

## Project setup
```
npm i
```

## Project start
```
npm run serve
```

## Project build
```
npm run build
```

## Developing app

### Create new component

```bash
npx hygen component new NameOfComponent
```

## Testing app

### Run Unit tests

```bash
npm run test:unit
```

### Run e2e tests

```bash
npm run test:e2e
```

## Lint app

### Run lint

```bash
npm run lint
```
