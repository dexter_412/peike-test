---
to: src/components/<%= name %>/__tests__/<%= name %>.spec.js
---
import { mount } from '@vue/test-utils';
import { toHaveNoViolations } from 'jest-axe';


import <%= name %> from '../<%= name %>';

expect.extend(toHaveNoViolations);

describe('<%= name %>', () => {
  test('accessibility', async () => {
    const wrapper = mount(<%= name %>);

    const results = await axe(wrapper.element);
    expect(results).toHaveNoViolations();
  });

  test('is a Vue instance', () => {
    const wrapper = mount(<%= name %>);

    expect(wrapper.vm).toBeTruthy();
  });
});
