// you need this to reformat the console.error
const { configureAxe } = require('jest-axe');

global.axe = configureAxe({
  rules: {
    // disabled landmark rules when testing isolated components.
    region: { enabled: false },
  },
});
