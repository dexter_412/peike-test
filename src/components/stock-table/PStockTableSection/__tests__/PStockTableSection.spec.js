import { mount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import { toHaveNoViolations } from 'jest-axe';
import table from '@/store/modules/table';

import PStockTableSection from '../PStockTableSection';

expect.extend(toHaveNoViolations);

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store({
  modules: {
    table,
  },
});

describe('PStockTableSection', () => {
  test('accessibility', async () => {
    const wrapper = mount(PStockTableSection, {
      store,
      localVue,
    });

    const results = await axe(wrapper.element);
    expect(results).toHaveNoViolations();
  });

  test('is a Vue instance', () => {
    const wrapper = mount(PStockTableSection, {
      store,
      localVue,
    });

    expect(wrapper.vm).toBeTruthy();
  });
});
