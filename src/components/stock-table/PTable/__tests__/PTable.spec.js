import { mount } from '@vue/test-utils';
import { toHaveNoViolations } from 'jest-axe';

import PTable from '../PTable';

expect.extend(toHaveNoViolations);

describe('PTable', () => {
  test('accessibility', async () => {
    const wrapper = mount(PTable);

    const results = await axe(wrapper.element);
    expect(results).toHaveNoViolations();
  });

  test('is a Vue instance', () => {
    const wrapper = mount(PTable);

    expect(wrapper.vm).toBeTruthy();
  });
});
