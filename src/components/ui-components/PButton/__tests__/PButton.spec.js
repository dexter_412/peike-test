import { mount } from '@vue/test-utils';
import { toHaveNoViolations } from 'jest-axe';

import PButton from '../PButton';

expect.extend(toHaveNoViolations);

describe('PButton', () => {
  test('accessibility', async () => {
    const wrapper = mount(PButton, {
      slots: {
        default: 'Button Title',
      },
    });

    const results = await axe(wrapper.element);
    expect(results).toHaveNoViolations();
  });

  test('is a Vue instance', () => {
    const wrapper = mount(PButton);

    expect(wrapper.vm).toBeTruthy();
  });
});
