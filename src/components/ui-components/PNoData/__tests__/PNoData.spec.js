import { mount } from '@vue/test-utils';
import { toHaveNoViolations } from 'jest-axe';

import PNoData from '../PNoData';

expect.extend(toHaveNoViolations);

describe('PNoData', () => {
  test('accessibility', async () => {
    const wrapper = mount(PNoData, {
      slots: {
        default: 'Button Title',
      },
    });

    const results = await axe(wrapper.element);
    expect(results).toHaveNoViolations();
  });

  test('is a Vue instance', () => {
    const wrapper = mount(PNoData);

    expect(wrapper).toBeTruthy();
  });
});
