import { mount } from '@vue/test-utils';
import { toHaveNoViolations } from 'jest-axe';

import RProgress from '../PProgress';

expect.extend(toHaveNoViolations);

describe('RProgress', () => {
  it('accessebility', async () => {
    const wrapper = mount(RProgress);

    const results = await axe(wrapper.element);
    expect(results).toHaveNoViolations();
  });

  test('is a Vue instance', () => {
    const wrapper = mount(RProgress);

    expect(wrapper).toBeTruthy();
  });
});
