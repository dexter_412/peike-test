import { mount } from '@vue/test-utils';
import { axe, toHaveNoViolations } from 'jest-axe';

import PTableCell from '../PTableCell';

expect.extend(toHaveNoViolations);

describe('PTableCell', () => {
  test('accessebility', async () => {
    const wrapper = mount(PTableCell);

    const results = await axe(wrapper.element);
    expect(results).toHaveNoViolations();
  });

  test('is a Vue instance', () => {
    const wrapper = mount(PTableCell);

    expect(wrapper.vm).toBeTruthy();
  });
});
