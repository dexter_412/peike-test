import { mount } from '@vue/test-utils';
import { toHaveNoViolations } from 'jest-axe';

import RRow from '../PTableRow';

expect.extend(toHaveNoViolations);

describe('RRow', () => {
  test('accessebility', async () => {
    const wrapper = mount(RRow);

    const results = await axe(wrapper.element);
    expect(results).toHaveNoViolations();
  });

  test('slot text', () => {
    const wrapper = mount(RRow, {
      slots: {
        default: 'test',
      },
    });

    expect(wrapper.text()).toBe('test');
  });
});
