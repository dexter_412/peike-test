import { toHaveNoViolations } from 'jest-axe';

import isNegative from '../isNegative';

expect.extend(toHaveNoViolations);

describe('isNegative', () => {
  test('bind', () => {
    const el = document.createElement('div');

    el.innerHTML = '123.123';
    isNegative.bind(el);
    expect(el.style.color).toBe('green');

    el.innerHTML = '-123.123';
    isNegative.bind(el);
    expect(el.style.color).toBe('red');
  });
});
