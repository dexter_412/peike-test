export default {
  bind(el) {
    // eslint-disable-next-line no-param-reassign
    el.style.color = Number(el.innerHTML) < 0 ? 'red' : 'green';
  },
};
