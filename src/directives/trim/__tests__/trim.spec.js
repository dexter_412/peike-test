import { toHaveNoViolations } from 'jest-axe';

import trim from '../trim';

expect.extend(toHaveNoViolations);

describe('trim', () => {
  test('bind', () => {
    const el = document.createElement('div');

    el.innerHTML = '123.123';
    trim.bind(el, { value: 2 });
    expect(el.innerHTML).toBe('123.12');
  });
});
