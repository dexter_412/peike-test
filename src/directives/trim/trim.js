export default {
  bind(el, { value }) {
    // eslint-disable-next-line no-param-reassign
    el.innerHTML = Number(el.innerHTML).toFixed(value);
  },
};
