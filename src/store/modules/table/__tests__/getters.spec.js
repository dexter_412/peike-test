import getters from '../getters';

describe('Reports store: Getters', () => {
  test('getSortedTableData', () => {
    const state = {
      tableData: {
        stocks: ['test'],
        start: [1],
        current: [1],
      },
    };

    expect(getters.getSortedTableData(state)).toEqual([
      {
        stock: 'test',
        start: 1,
        current: 1,
      },
    ]);
  });
});
