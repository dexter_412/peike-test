import * as types from '../mutation-types';
import mutations from '../mutations';

describe('Table store: Mutations', () => {
  test('SET_TABLE_DATA', () => {
    const state = {
      tableData: {},
      isLoadingTableData: true,
    };

    mutations[types.SET_TABLE_DATA](state, [
      {
        stocks: ['IBM', 'AAPL', 'AMZN'],
        start: [141.78, 237.15, 1727.41],
        current: [131.67, 287.48, -1727.4181111],
      },
    ]);
    expect(state.isLoadingTableData).toBeFalsy();
    expect(state.tableData).toEqual([
      {
        stocks: ['IBM', 'AAPL', 'AMZN'],
        start: [141.78, 237.15, 1727.41],
        current: [131.67, 287.48, -1727.4181111],
      },
    ]);
  });

  test('START_LOAD_TABLE_DATA', () => {
    const state = {
      isLoadingTableData: false,
    };

    mutations[types.START_LOAD_TABLE_DATA](state);
    expect(state.isLoadingTableData).toBeTruthy();
  });

  test('SET_ERROR_LOADING', () => {
    const state = {
      isLoadingTableDataError: false,
    };

    mutations[types.SET_ERROR_LOADING](state);
    expect(state.isLoadingTableDataError).toBeTruthy();
  });
});
