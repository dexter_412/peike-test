/**
 * @module store/table/actions
 *
 * @version 1.0.0
 */

import payload from '@/mocData';
import simulateAsyncReq from '@/plugins/getDataFunc';
import * as types from './mutation-types';

/**
 * Action: Get table data
 *
 * @param {object} commit - vuex commit
 *
 * @since Version 1.0.0
 */

const getTableData = async ({ commit }) => {
  commit(types.START_LOAD_TABLE_DATA);
  try {
    const data = await simulateAsyncReq(payload);
    commit(types.SET_TABLE_DATA, data);
  } catch (e) {
    commit(types.SET_ERROR_LOADING);
    console.error(e);
  }
};

export default {
  getTableData,
};
