/**
 * @module store/table/getters
 *
 * @version 1.0.0
 */

/**
 * Getter: Get sorted table data
 *
 * @param {object} state - vuex state
 *
 * @since Version 1.0.0
 */

const getSortedTableData = (state) => {
  if (!state.tableData.stocks) return [];
  const items = [];

  state.tableData.stocks.forEach((stock, index) => {
    items.push({
      stock,
      start: state.tableData.start[index],
      current: state.tableData.current[index],
    });
  });

  return items.sort((a, b) => (a.stock > b.stock ? 1 : -1));
};

export default {
  getSortedTableData,
};
