/**
 * @module store/table/mutations
 *
 * @version 1.0.0
 */

import * as types from './mutation-types';

export default {
  /**
   * Mutation: Set table data
   *
   * @param {object} state - vuex state
   * @param {array} tableData - tableData
   *
   * @since Version 1.0.0
   */

  [types.SET_TABLE_DATA](state, tableData) {
    state.tableData = tableData;
    state.isLoadingTableData = false;
  },

  /**
   * Mutation: Start load table data
   *
   * @param {object} state - vuex state
   *
   * @since Version 1.0.0
   */

  [types.START_LOAD_TABLE_DATA](state) {
    state.isLoadingTableData = true;
    state.isLoadingTableDataError = false;
  },

  /**
   * Mutation: Set error loading table data
   *
   * @param {object} state - vuex state
   *
   * @since Version 1.0.0
   */

  [types.SET_ERROR_LOADING](state) {
    state.isLoadingTableData = false;
    state.isLoadingTableDataError = true;
    state.tableData = {};
  },
};
