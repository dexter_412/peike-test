describe('Reports View', () => {
  it('page title should exist', () => {
    cy.visit('');
    cy.contains('[data-test=page-title]', 'Test');
  });

  it('check load data button', () => {
    cy.visit('');
    cy.get('[data-test=button-load-data]')
      .click();
  });
});
